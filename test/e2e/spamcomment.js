const config = require('../../nightwatch.conf.js');

module.exports = { // addapted from: https://git.io/vodU0
  '@tags': ['express'],
  'Eesti Ekspress': function (browser) {
    browser
      .url('http://ekspress.delfi.ee')
      .pause(1000)
      .waitForElementVisible('body')
      .pause(1000)
      .resizeWindow(1280, 800)
      .saveScreenshot(`${config.imgpath(browser)}express1_1.png`)
      .pause(1000)

      .click('div[class="headline__picture"]')
      .pause(1000)
      .click('a[class="article__comments article__comments--big icon"]') 
      .saveScreenshot(`${config.imgpath(browser)}express1_2.png`)
      .pause(1000)
      .click('a[class="comment-thread-switcher-list-a comment-thread-switcher-list-a-right thread-unselected"]')     
      .pause(1000)
      .click('div[class="comments-form-add-comment"]')
      .saveScreenshot(`${config.imgpath(browser)}express1_3.png`)
      .pause(1000)
      .setValue('input[class="comment-form-title show-if-anon"]', "test")
      .pause(1000)
      .setValue('textarea[class="comment-form-body"]', "testtesttest123")
      .pause(1000)
      .click('input[class="btn"]')
      .pause(1000)
      .saveScreenshot(`${config.imgpath(browser)}express1_4.png`)
      .pause(1000);

    browser
      .pause(1000)
      .end();
  },
};
